FROM zenko/cloudserver

COPY ./locationConstraint.json /usr/src/app/

CMD [ "yarn", "start" ]